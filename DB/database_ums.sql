-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2017 at 09:13 AM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_ums`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_account`
--

CREATE TABLE `tb_account` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `debit` varchar(255) NOT NULL,
  `credit` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_applicant`
--

CREATE TABLE `tb_applicant` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `ssc_roll` varchar(255) NOT NULL,
  `ssc_reg` varchar(255) NOT NULL,
  `ssc_board` varchar(255) NOT NULL,
  `ssc_session` varchar(255) NOT NULL,
  `ssc_result` varchar(255) NOT NULL,
  `ssc_subject` text NOT NULL,
  `hsc_roll` varchar(255) NOT NULL,
  `hsc_reg` varchar(255) NOT NULL,
  `hsc_board` varchar(255) NOT NULL,
  `hsc_session` varchar(255) NOT NULL,
  `hsc_result` varchar(255) NOT NULL,
  `hsc_subject` text NOT NULL,
  `marks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_applicant`
--

INSERT INTO `tb_applicant` (`id`, `userid`, `ssc_roll`, `ssc_reg`, `ssc_board`, `ssc_session`, `ssc_result`, `ssc_subject`, `hsc_roll`, `hsc_reg`, `hsc_board`, `hsc_session`, `hsc_result`, `hsc_subject`, `marks`) VALUES
(1, '34', '129878', '1000897', 'Dhaka', '2010-11', '4.50', 'Bangla, English, Mathematics, Physics, Chemistry, Biology, ICT', '980976', '900076', 'Dhaka', '2011-12', '4.60', 'Bangla, English, Mathematics, Physics, Chemistry, Biology, ICT', 'Admitted'),
(2, '35', '129868', '1011897', 'Dhaka', '2010-11', '4.80', 'Bangla, English, Mathematics, Physics, Chemistry, Biology, ICT', '982276', '9011076', 'Dhaka', '2011-12', '4.90', 'Bangla, English, Mathematics, Physics, Chemistry, Biology, ICT', 'Admitted'),
(3, '36', '0909 000 00', '1234321', 'DHAKA', '4949-94', '5.00', 'A, B C D E F G H I J K L M N O P Q R S T', '9876543434', '222343434', 'DHAKA', '9898-09', '2.50', 'A, B C D E F G H I J K L M N O P Q R S T', ''),
(4, '37', '090909', '878787', 'KHULNA', '1234-12', '5.00', 'SKDFJ, LDKS JF , J LK, LIJ L, LSKJF ,L;KJ.', '343434333', '222444', 'CHITTAGONG', '9898-09', '2.50', 'SKDFJ, LDKS JF , J LK, LIJ L, LSKJF ,L;KJ.', 'Admitted'),
(5, '39', '34344340456', '09090909456', 'CHITTAGONG', '4949-94', '5.00', 'tfg dsfgfds gsdfg sdfg sdfg sdf gdsfgdsfg', '222343443', '2323434', 'CHITTAGONG', '5676-98', '3.50', 'sadf asd fasd fasdf ads', 'Admitted');

-- --------------------------------------------------------

--
-- Table structure for table `tb_canteen`
--

CREATE TABLE `tb_canteen` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `foods` varchar(255) NOT NULL,
  `bills` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_checkout`
--

CREATE TABLE `tb_checkout` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `checkout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_food`
--

CREATE TABLE `tb_food` (
  `id` int(11) NOT NULL,
  `foodname` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_hall`
--

CREATE TABLE `tb_hall` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `hallname` varchar(255) NOT NULL,
  `floorno` varchar(255) NOT NULL,
  `roomno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `father_occupation` varchar(255) NOT NULL,
  `father_income` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mother_name` varchar(255) NOT NULL,
  `mother_occupation` varchar(255) NOT NULL,
  `mother_income` varchar(255) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`id`, `userid`, `firstname`, `lastname`, `fullname`, `email`, `phone`, `father_name`, `father_occupation`, `father_income`, `mother_name`, `mother_occupation`, `mother_income`, `birthdate`, `gender`, `address1`, `address2`) VALUES
(10, '34', 'oioioi 24', 'Hossain24', 'FatemaIslam', 'student@one.com', '+990 9090 909090', 'A. Rahman', 'N/A', '2017-12-15 19:02:11', 'Halima Khatun1', 'N/A', 'N/A', '2017-12-25', 'Female', 'dhaka, bangladesh', 'norail'),
(11, '35', 'oioioi ', 'one', 'studentOne', 'islam2311@live.com', '+009 9090 090909', 'FATHER 01', 'GOVT. EMPLOYEE', '2017-12-15 19:02:35', 'Halima Khatun1', 'House Holder', 'N/A', '2017-12-20', 'Male', '98, DHAKA', 'BANGLADESH'),
(12, '36', 'Anowar', 'Shorif', 'Anowar Shorif', 'anowar@mail.com', '33', 'Abdullah', 'FARMER', '0000-00-00 00:00:00', 'Anowara', 'House Holder', '998099', '2017-12-20', 'Male', 'Bogra, Bangladesh', 'Dhaka, Bangladesh'),
(13, '37', 'Shamima', 'Akter', 'Shamima Akter', 'jui@mail.com', '+990 8987 654321', 'Shah Jalal Khan', 'Business', '0000-00-00 00:00:00', 'Anowara Khatun', 'HOUSE WIFE', 'N/A', '2017-12-20', 'Female', 'Kolatia, Dhaka', 'Savar, Dhaka'),
(14, '38', 'STUDENT ASDF DSA', 'Hossain ASDF ASDF ', ' StudentTwo SDAFDSA ', 'islam2311@live.com', '+009 9090 090909', 'FATHER 01', 'N/A', '0000-00-00 00:00:00', 'Halima Khatun', 'N/A', 'N/A', '2017-12-19', 'Male', 'SADF AFDSAASD', 'SDF ASDFASD'),
(15, '39', 'someone asdf a', 'someone df', 'someone father', 'student@one.com', '+998 8989 767676', 'A. Rahman1', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-21', 'Male', 'da fasd fasdf asdf ', 'asdf asdasdf asd fasdf'),
(16, '41', 'bithi', 'bithi', 'bithi', 'student@one.com', '+009 9090 090909', 'A. Rahman', 'N/A', '0000-00-00 00:00:00', 'Halima Khatun', 'N/A', 'N/A', '2017-12-01', 'Female', 'fgtyhtrfgghhgf', 'fgghhjjkjk');

-- --------------------------------------------------------

--
-- Table structure for table `tb_student`
--

CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  `idno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_student`
--

INSERT INTO `tb_student` (`id`, `userid`, `department`, `session`, `idno`) VALUES
(1, '8', 'Bachelor of Business Administration - BBA', '', 'Hall-II Male'),
(2, '8', 'Bachelor of Business Administration - BBA', '', 'Hall-II Male'),
(3, '8', 'Applied Physics', '', 'Hall-IV Female'),
(4, '9', 'Physics', '', 'Hall-II Male'),
(5, '1', 'Chemistry', '', 'AC-001'),
(6, '1', 'Computer Science & Engineering -CSE', '', 'AC-001'),
(7, '1', 'Bachelor of Business Administration - BBA', '', 'AC-001'),
(8, '1', 'Bachelor of Business Administration - BBA', '', 'AC-001'),
(9, '1', 'Bachelor of Business Administration - BBA', '', 'AC-001'),
(10, '2', 'Physics', '', 'AC-002'),
(11, '4', 'Chemistry', '', 'AC-003'),
(12, '5', 'Population Science', '', 'AC-004');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `role`) VALUES
(1, 'Admin', '21232f297a57a5a743894a0e4a801fc3', 'Admin'),
(34, 'student', 'cd73502828457d15655bbd7a63fb0bc8', 'Student'),
(35, '22', 'b6d767d2f8ed5d21a44b0e5886680cb9', 'Student'),
(36, 'Anwar', '4124bc0a9335c27f086f24ba207a4912', 'Student'),
(37, 'jui', 'b5de674a38a691fb24d04233e8aa498b', 'Student'),
(38, 'DELWAR', '350bfcb1e3cfb28ddff48ce525d4f139', 'Accounce'),
(39, 'someone', '3691308f2a4c2f6983f2880d32e29c84', 'Student'),
(40, 'Farhana Alam', '202cb962ac59075b964b07152d234b70', 'Author'),
(41, 'bithi', '202cb962ac59075b964b07152d234b70', 'Author');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_account`
--
ALTER TABLE `tb_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_applicant`
--
ALTER TABLE `tb_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_canteen`
--
ALTER TABLE `tb_canteen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_food`
--
ALTER TABLE `tb_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_hall`
--
ALTER TABLE `tb_hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_student`
--
ALTER TABLE `tb_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_account`
--
ALTER TABLE `tb_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_applicant`
--
ALTER TABLE `tb_applicant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tb_canteen`
--
ALTER TABLE `tb_canteen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_food`
--
ALTER TABLE `tb_food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_hall`
--
ALTER TABLE `tb_hall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_profile`
--
ALTER TABLE `tb_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tb_student`
--
ALTER TABLE `tb_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
