<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('administration_model'); 
		$this->load->model('student_model'); 
	}

/*SHOW APPLICANTS LIST*/
	public function ApplicantList()
	{
		$data = array(); 

		$data['applicants'] = $this->administration_model->Applicant_list($data);
		
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('Administration/applicant_list.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	} 

/*ADD STUDENTS MARKS..*/
	public function MarksUpdate(){
		$data = array(); 
		$data['id'] = $this->input->post('id'); 
		$data['marks'] = $this->input->post('marks');
		
		$this->administration_model->Add_Marks($data);
		redirect('Administration/ApplicantList'); 
	}

/*SHOW ALL STUDENTS..*/
	public function StudentList(){
		$data = array(); 
		$data['students'] = $this->student_model->studentList($data); 
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('Administration/student_list.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	}


}
