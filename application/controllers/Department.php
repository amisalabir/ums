<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Department extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('department_model'); 
		$this->load->model('administration_model'); 
	}

	public function ApplicantList()
	{
		$data = array(); 

		$data['applicants'] = $this->administration_model->Applicant_list($data);
		
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('Department/applicant_list.php',$data, TRUE);  
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	}

	public function AddStudent(){
		$data = array(); 
		$data['userid'] = $this->input->post('userid'); 
		$data['department'] = $this->input->post('department'); 
		$data['session'] = $this->input->post('session'); 
		$data['idno'] = $this->input->post('idno'); 
		
		$this->department_model->Add_Student($data);
		$this->department_model->confirmadmission($data);
		echo '<script>alert("Successfully Admitted !");</script>'; 
		redirect('Department/ApplicantList',$sdata); 
	}

}
