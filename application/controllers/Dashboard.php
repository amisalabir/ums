<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	/***
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 ***/
	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('administration_model'); 
		$this->load->model('student_model'); 
	}
	
	public function index()
	{

		if(!$this->session->userdata('userlogin')){
			redirect('Login');
		}
		
		//$data = array(); 
		$this->home();

	}	

	public function home(){

		$data = array();
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); // load admin header from includes....		
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); // load admin menu from includes...	

		if($this->session->role == 'Student'){
			$data['userid'] = $this->session->userdata('id'); 
			$data['profiles'] = $this->student_model->profile($data); 
			$data['applicantscheck'] = $this->student_model->Applicant_Check($data);

			$data['content'] = $this->load->view('Student/home.php',$data, TRUE); // load admin dashboard from files....		

		}else{
			$data['content'] = $this->load->view('Administration/home.php',$data, TRUE); // load admin dashboard from files....	
		}

		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE); // load footer from includes......
		$this->load->view('dashboard', $data);

	}

}
