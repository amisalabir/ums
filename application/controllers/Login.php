<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
	}

	public function index()
	{
		$this->load->view('login');
	}//login form... 

	public function RegisterForm()
	{
		$data = array(); 
		$data['username'] = $this->input->post('username'); 
		$password         = $this->input->post('password'); 
		$cpassword        = $this->input->post('cpassword'); 
		$data['role']     = $this->input->post('role'); 

		if(empty($data['username']) OR empty($password) OR empty($cpassword) OR empty($data['role'])){

			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Worng Insertion !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 	

		}elseif($password == $cpassword){

			$data['password'] = md5($password); //password encrypting...
			if($data['role'] == 'Author')
			{
				$this->login_model->saveUser($data);
				$getUser = $this->login_model->getUesr($data);
				if($getUser){ 
					$sdata['userid'] = $getUser->id; 
					$this->session->set_flashdata($sdata);
				} 
				redirect('Login/AuthorRegister',$sdata); 
			}
			elseif($data['role'] == 'Student')
			{
				$this->login_model->saveUser($data);
				$getUser = $this->login_model->getUesr($data);
				if($getUser){ 
					$sdata['userid'] = $getUser->id; 
					$this->session->set_flashdata($sdata);
				} 
				redirect('Login/StudentRegister',$sdata);
			}
		}else{
			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Password Not Matched !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}
	}

	public function AuthorRegister()
	{
		$this->load->view('author');
	}//Teacher register form...  

	//SAME AUTHOR TO DB...
	public function saveAuthor(){
		$data = array(); 
		
		$data['userid'] = $this->input->post('userid'); //userid for foreighn key.. 

		$data['firstname'] = $this->input->post('firstname'); 
		$data['lastname'] = $this->input->post('lastname'); 
		$data['fullname'] = $this->input->post('fullname'); 
		$data['email'] = $this->input->post('email'); 
		$data['phone'] = $this->input->post('phone'); 

		$data['father_name'] = $this->input->post('father_name'); 
		$data['father_occupation'] = 'N/A'; 
		$data['father_income'] = 'N/A'; 
		$data['mother_name'] = $this->input->post('mother_name'); 
		$data['mother_occupation'] = 'N/A';
		$data['mother_income'] = 'N/A'; 

		$data['birthdate'] = $this->input->post('birthdate'); 
		$data['gender'] = $this->input->post('gender'); 
		$data['address1'] = $this->input->post('address1');
		$data['address2'] = $this->input->post('address2'); 

		if(empty($data['userid']) OR empty($data['firstname']) OR empty($data['lastname']) OR empty($data['fullname']) OR empty($data['email']) OR empty($data['phone']) OR empty($data['father_name']) OR empty($data['mother_name']) OR empty($data['birthdate']) OR empty($data['gender']) OR empty($data['address1']))
		{
			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Failed; Try Again !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}else{
			$this->login_model->saveProfile($data);

			$sdata = array();
			$sdata['msg'] = '<span style="color:green;">Registration Successfull; Login & Apply Now !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}
	}

	//STUDENT REGISTRATION FORM... 
	public function StudentRegister()
	{
		$this->load->view('student');
	}//Student Register Form... 

	//SAVE STUDENT TO DB..
	public function saveStudent(){
		$data = array(); 
		
		$data['userid'] = $this->input->post('userid'); //userid for foreighn key.. 

		$data['firstname'] = $this->input->post('firstname'); 
		$data['lastname'] = $this->input->post('lastname'); 
		$data['fullname'] = $this->input->post('fullname'); 
		$data['email'] = $this->input->post('email'); 
		$data['phone'] = $this->input->post('phone'); 

		$data['father_name'] = $this->input->post('father_name'); 
		$data['father_occupation'] = $this->input->post('father_occupation'); 
		$data['father_income'] = $this->input->post('mother_income'); 
		$data['mother_name'] = $this->input->post('mother_name'); 
		$data['mother_occupation'] = $this->input->post('mother_occupation');
		$data['mother_income'] = $this->input->post('mother_income'); 

		$data['birthdate'] = $this->input->post('birthdate'); 
		$data['gender'] = $this->input->post('gender'); 
		$data['address1'] = $this->input->post('address1');
		$data['address2'] = $this->input->post('address2'); 

		if(empty($data['userid']) OR empty($data['firstname']) OR empty($data['lastname']) OR empty($data['fullname']) OR empty($data['email']) OR empty($data['phone']) OR empty($data['father_name']) OR empty($data['mother_name']) OR empty($data['birthdate']) OR empty($data['gender']) OR empty($data['address1']))
		{
			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Failed; Try Again !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}else{
			$this->login_model->saveProfile($data);

			$sdata = array();
			$sdata['msg'] = '<span style="color:green;">Registration Successfull; Login & Apply Now !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}
	}

	public function UserLoginForm(){
		$data = array();
		$data['username']  = $this->input->post('username');
		$data['password']  = $this->input->post('password');

		$username = $data['username']; 
		$password = $data['password'];

		if(empty($username) OR empty($password)){
			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Invalid Username Or Password !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Login',$sdata); 
		}else{
			$this->login_model->checkUser($data);

			$sdata = array();
			$sdata['msg'] = '<span style="color:green;">Wellcome to Dashboard !</span>'; 


			$check = $this->login_model->checkUser($data);
			if($check){
				$sdata = array(); 
				$sdata['id'] = $check->id; 
				$sdata['username'] = $check->username; 
				$sdata['role'] = $check->role;
				$sdata['userlogin'] = TRUE; 
				$this->session->set_userdata($sdata);//set as session... 
				redirect('Dashboard');
			}else{
				$sdata = array();
				$sdata['msg'] = '<span style="color:red;">Invalid Username Or Password !</span>'; 
				$this->session->set_flashdata($sdata);
				redirect('Login',$sdata); 
			}
		}
	}/*End of Login Form Classes*/


	public function Logout(){
		//$uid = $this->session->id;
		//$this->login_model->userLoginOFF($uid);//update login status.OFF..
		$this->session->unset_userdata($id);
		$this->session->unset_userdata('userlogin',FALSE);
		$this->session->sess_destroy(); 
		redirect('Login'); 
	}

}
