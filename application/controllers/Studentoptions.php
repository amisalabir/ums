<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Studentoptions extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('administration_model'); 
		$this->load->model('student_model'); 
	}

	public function Student()
	{
		$data = array(); 

		$data['applicants'] = $this->administration_model->Applicant_list($data);
		
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('Student/applicant.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	} 
	public function ApplyNow(){
		$this->load->view('apply');
	}

	public function SaveApplication(){
		$data = array(); 
		
		$data['userid'] = $this->input->post('userid'); //userid for foreighn key.. 

		$data['ssc_roll'] = $this->input->post('ssc_roll'); 
		$data['ssc_reg'] = $this->input->post('ssc_reg'); 
		$data['ssc_board'] = $this->input->post('ssc_board'); 
		$data['ssc_session'] = $this->input->post('ssc_session'); 
		$data['ssc_result'] = $this->input->post('ssc_result'); 
		$data['ssc_subject'] = $this->input->post('ssc_subject'); 

		$data['hsc_roll'] = $this->input->post('hsc_roll'); 
		$data['hsc_reg'] = $this->input->post('hsc_reg'); 
		$data['hsc_board'] = $this->input->post('hsc_board'); 
		$data['hsc_session'] = $this->input->post('hsc_session'); 
		$data['hsc_result'] = $this->input->post('hsc_result'); 
		$data['hsc_subject'] = $this->input->post('hsc_subject'); 

		if(empty($data['userid']))
		{
			$sdata = array();
			$sdata['msg'] = '<span style="color:red;">Failed; Try Again !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Studentoptions/ApplyNow',$sdata); 
		}else{
			$this->student_model->SaveApplication($data);

			$sdata = array();
			$sdata['msg'] = '<span style="color:green;">Registration Successfull; Login & Apply Now !</span>'; 
			$this->session->set_flashdata($sdata);
			redirect('Dashboard',$sdata); 
		}
	}


}
