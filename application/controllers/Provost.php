<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Provost extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
		$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
		$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
		$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
		$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('Provost_Model'); 
	}

	public function ApplicantList()
	{
		$data = array(); 

		$data['applicants'] = $this->Provost_Model->Applicant_list($data);
		$data['applicant_amount'] = $this->Provost_Model->Applicant_Quantity($data); 
		

		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('Provost/applicant_list.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	}


	public function AddStudent(){
		$data = array(); 
		$data['profileid'] = $this->input->post('profileid'); 
		$data['department'] = $this->input->post('department'); 
		$data['hall'] = $this->input->post('hall'); 

		$this->Department_Model->admitted($data['profileid']);

		$this->Department_Model->Add_Student($data);
		echo '<script>alert("Successfully Admitted !");</script>'; 
		redirect('Department/ApplicantList',$sdata); 
	}

}
