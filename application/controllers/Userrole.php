<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Userrole extends CI_Controller {

	public function __construct()
	{
		parent::__construct(); 
		
		/*..cache removal code........
			$this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
			$this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
			$this->output->set_header('Cache-Control: post-check=0, pre-check=0', false);
			$this->output0->set_header('Pragma: no-cache');
		.*/
		$this->load->model('login_model');
		$this->load->model('user_model'); 
	}

	public function UserList()
	{
		$data = array(); 

		$data['users'] = $this->user_model->user_list($data); 
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('User/userlist.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	} 

	public function ShowDetails($id){
		$data = array(); 
		$data['userid'] = $id; 
		$data['profiles'] = $this->user_model->show_details($data); 
		$data['header'] = $this->load->view('Included/header.php',$data, TRUE); 	
		$data['menu'] = $this->load->view('Included/menu.php',$data, TRUE); 
		$data['content'] = $this->load->view('User/profile.php',$data, TRUE); 
		$data['footer'] = $this->load->view('Included/footer.php',$data, TRUE);
		$this->load->view('dashboard', $data);
	}
/*========================================*/

	public function Author($id){
		$this->user_model->Author($id); 
		redirect('Userrole/UserList'); 
	}
	public function Accounce($id){
		$this->user_model->Accounce($id); 
		redirect('Userrole/UserList'); 
	}
	public function Department($id){
		$this->user_model->Department($id); 
		redirect('Userrole/UserList'); 
	}
	public function Provost($id){
		$this->user_model->Provost($id); 
		redirect('Userrole/UserList'); 
	}
	public function Canteen($id){
		$this->user_model->Canteen($id); 
		redirect('Userrole/UserList'); 
	}
	public function Librarian($id){
		$this->user_model->Librarian($id); 
		redirect('Userrole/UserList'); 
	}

}
