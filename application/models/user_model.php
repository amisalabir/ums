<?php
	class User_Model extends CI_Model{

		public function user_list($data){
			$this->db->select('*');
			$this->db->from('tb_user');
			$result =$this->db->get();
			$result = $result->result(); 
			return $result;
		}

		public function show_details($data){
			$this->db->select('*'); 
			$this->db->from('tb_profile'); 
			$this->db->where('userid',$data['userid']); 	
			$result = $this->db->get(); 
			$result = $result->row(); 
			return $result; 
		}



		public function Author($id){
			$this->db->set('role','Author');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}
		public function Accounce($id){
			$this->db->set('role','Accounce');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}
		public function Department($id){
			$this->db->set('role','Department');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}
		public function Provost($id){
			$this->db->set('role','Provost');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}
		public function Canteen($id){
			$this->db->set('role','Canteen Manager');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}
		public function Librarian($id){
			$this->db->set('role','Librarian');
			$this->db->where('id',$id);
			$this->db->update('tb_user'); 
		}

	}
?>