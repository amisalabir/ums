<?php
	class Provost_Model extends CI_Model{

		public function Applicant_list($data){
			$this->db->select('*');
			$this->db->from('tb_profile');
			$this->db->where('approved_by','1'); 
			$this->db->where('admitted','1'); 
			$result =$this->db->get();
			$result = $result->result(); 
			return $result;
		}

		public function Applicant_Hall($id){
			$this->db->select('hall');
			$this->db->from('tb_student');
			$this->db->where('profileid',$id);
			$result = $this->db->get(); 
			$result = $result->rows();
			return $result;
		}

		public function Applicant_Quantity($data){
			$this->db->select('*'); 
			$this->db->from('tb_profile'); 	
			$this->db->where('approved_by','1');
			$this->db->where('admitted','1');
			$result = $this->db->get(); 
			$result = $result->num_rows(); 
			return $result; 
		}

		public function Add_Student($data){
			$this->db->insert('tb_student',$data); 
		}
		public function admitted($id){
			$this->db->set('admitted',1);
			$this->db->where('id',$id);
			$this->db->update('tb_profile');
		}

		public function Approved($data){
			$this->db->set('approved_by','1');
			$this->db->where('id',$data['id']);
			$this->db->update('tb_profile'); 
		}
		public function Decline($data){
			$this->db->set('approved_by','0');
			$this->db->where('id',$data['id']);
			$this->db->update('tb_profile'); 
		}
	}
?>