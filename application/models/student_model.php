<?php
	class Student_Model extends CI_Model{
/*CHECK APPLICANT*/
		public function Applicant_Check($data){
			$this->db->select('*');
			$this->db->from('tb_applicant');
			$this->db->where('userid',$data['userid']);
			$this->db->where('marks','!=NULL'); 
			$qresult = $this->db->get();
			$result = $qresult->row();
			return $result; 
		}
/*CHECK APPLICANT*/
		public function applicantById($id){
			$this->db->select('*');
			$this->db->from('tb_applicant');
			$this->db->where('userid',$id);
			$qresult = $this->db->get();
			$result = $qresult->row();
			return $result; 
		}
/*APPLICANT PROFILE DATA*/
		public function profile($data){
			$this->db->select('*');
			$this->db->from('tb_profile');
			$this->db->where('userid',$data['userid']);
			$qresult = $this->db->get();
			$result = $qresult->row();
			return $result; 
		}

/*APPLICANT PROFILE DATA*/
		public function profileById($id){
			$this->db->select('*');
			$this->db->from('tb_profile');
			$this->db->where('userid',$id);
			$qresult = $this->db->get();
			$result = $qresult->row();
			return $result; 
		}

/*CHECK APPLICANT*/
		public function checkapplicant($data){
			$this->db->select('*'); 
			$this->db->from('tb_applicant'); 	
			$result = $this->db->get(); 
			$result = $result->num_rows(); 
			return $result; 
		}

/*ADD APPLICANT MARKS TO APPLICANT TABLE*/
		public function Add_Marks($data){
			$this->db->set('marks',$data['marks']);
			$this->db->where('id',$data['id']);
			$this->db->update('tb_applicant'); 
		}

/*SAVE APPLICATION*/
		public function SaveApplication($data){
			$this->db->insert('tb_applicant',$data); 
		}

/*SHOW ADMITTED STUDENTS*/
		public function studentList($data){
			$this->db->select('*');
			$this->db->from('tb_student');
			$result = $this->db->get(); 
			$result = $result->result();
			return $result; 
		}

	}
?>