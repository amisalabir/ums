<?php
	class Administration_Model extends CI_Model{

/*ALL APPLICANT LIST FOR TABLE*/
		public function Applicant_list($data){
			$this->db->select('*');
			$this->db->from('tb_applicant');
			$result =$this->db->get();
			$result = $result->result(); 
			return $result;
		}

/*APPLICANT PROFILE DATA*/
		public function applicantprofile($userid){
			$this->db->select('*');
			$this->db->from('tb_profile');
			$this->db->where('userid',$userid);
			$qresult = $this->db->get();
			$result = $qresult->row();
			return $result; 
		}

/*CHECK APPLICANT*/
		public function checkapplicant($data){
			$this->db->select('*'); 
			$this->db->from('tb_applicant'); 	
			$result = $this->db->get(); 
			$result = $result->num_rows(); 
			return $result; 
		}

/*ADD APPLICANT MARKS TO APPLICANT TABLE*/
		public function Add_Marks($data){
			$this->db->set('marks',$data['marks']);
			$this->db->where('id',$data['id']);
			$this->db->update('tb_applicant'); 
		}

		public function Approved($data){
			$this->db->set('approved_by','1');
			$this->db->where('id',$data['id']);
			$this->db->update('tb_profile'); 
		}
		public function Decline($data){
			$this->db->set('approved_by','0');
			$this->db->where('id',$data['id']);
			$this->db->update('tb_profile'); 
		}
	}
?>