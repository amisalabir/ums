<?php
	class Department_Model extends CI_Model{

		public function Applicant_list($data){
			$this->db->select('*');
			$this->db->from('tb_applicant');
			$this->db->where('marks','!=NULL');
			$this->db->where('marks','!=Admitted'); 
			$result =$this->db->get();
			$result = $result->result(); 
			return $result;
		}


		public function Add_Student($data){
			$this->db->insert('tb_student',$data); 		
		}

		public function confirmadmission($data){
			$this->db->set('marks','Admitted');
			$this->db->where('userid',$data['userid']);
			$this->db->update('tb_applicant');
		}

		public function Decline($data){
			$this->db->set('approved_by','0');
			$this->db->where('id',$data['id']);
			$this->db->update('tb_profile'); 
		}
	}
?>