<div class="table-responsive">
<table id="mytableId" class="table table-bordered table-responsive">
    <thead>
        <tr style="color:white;">
            <th style="width:;">SL</th>
            <th style="width:;">Student Info</th>
            <th style="width:;">Description</th>
            <th style="width:;">Action</th>   
        </tr>
    </thead>
    <tbody>

<?php 
    $i = 0;
    foreach ($applicants as $applicant) {
        $i++; 
?>
        <tr>
            <td><?php echo $i; ?></td>
            <td>
                <b><?php echo $applicant->fullname; ?></b><br/>
            </td>
            <td>
                <b>SSC:</b> Roll- <?php echo $applicant->ssc_roll; ?>&nbsp; 
                     Reg- <?php echo $applicant->ssc_reg; ?>&nbsp; 
                     Board- <?php echo $applicant->ssc_board; ?>&nbsp; 
                     Roll- <?php echo $applicant->ssc_result; ?>
                <br/>
                <b>HSC:</b> Roll- <?php echo $applicant->hsc_roll; ?>&nbsp; 
                     Reg- <?php echo $applicant->hsc_reg; ?>&nbsp; 
                     Board- <?php echo $applicant->hsc_board; ?>&nbsp; 
                     Roll- <?php echo $applicant->hsc_result; ?>
            </td>
            <td>
                <?php if($applicant->approved_by == 1){ ?>
                <a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#confirmadmission<?php echo $applicant->id; ?>">Confirm Hall</a>
                    <!-- Modal -->
                    <form action="<?php echo base_url('index.php/Department/AddStudent'); ?>" method="post">
                    <div class="modal fade" id="confirmadmission<?php echo $applicant->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h4 class="modal-title" id="myModalLabel"><?php echo $applicant->fullname; ?></h4>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="profileid" value="<?php echo $applicant->id; ?>">
                            <br/>
                            <select name="hall" class="form-control">
                              <option value="">Select Hall !</option>
                              <option value="Hall-I Male">Hall-I Male</option>
                              <option value="Hall-II Male">Hall-II Male</option>
                              <option value="Hall-III Female">Hall-III Female</option>
                              <option value="Hall-IV Female">Hall-IV Female</option>
                            </select>
                            <br/>
                            <input type="text" class="form-control" placeholder="Floor / Level"><br/>
                            <input type="text" class="form-control" placeholder="Room No">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Confirm Hall</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    </form>
                <?php }elseif($applicant->admitted == '1'){ echo '<span class="alert-info"><b>Admitted</b></span>'; } ?></td>
        </tr> 
<?php 
    }//applicant list..
?>


    </tbody>
</table>
</div>