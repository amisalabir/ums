<div class="table-responsive">
<table id="mytableId" class="table table-bordered table-responsive">
    <thead>
        <tr style="color:white;">
            <th>ID</th>
            <th>Student Name</th>
            <th>Department</th>
            <th>Session</th>   
        </tr>
    </thead>
    <tbody>

<?php 
    foreach ($students as $allstudent) {
?>
        <tr>
            <td><?php echo $allstudent->idno; ?></td>
            <td>

            <?php 
            	$userid = $allstudent->userid;
            	$getprofile = $this->student_model->profileById($userid); 
            	if(isset($getprofile)){ echo $getprofile->fullname; }
            ?>
            </td>
            <td><?php echo $allstudent->department; ?></td>
            <td><?php echo $allstudent->session; ?></td>
        </tr> 
<?php 
    }//END OF FOREACH STUDENTS..
?>
    </tbody>
</table>
</div>