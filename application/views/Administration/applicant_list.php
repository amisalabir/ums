<div class="table-responsive">
<table id="mytableId" class="table table-bordered table-responsive">
    <thead>
        <tr style="color:white;">
            <th style="width:;">SL</th>
            <th style="width:;">Applicant Info</th>
            <th style="width:;">Marks</th>   
        </tr>
    </thead>
    <tbody>

<?php 
    $i = 0;
    foreach ($applicants as $applicant) {
        $i++; 
?>
        <tr>
              <?php 
                  $userid = $applicant->userid; 
                  $getprofile = $this->administration_model->applicantprofile($userid); 
              ?>
            <td style="line-height: 15vh;"><?php echo $i; ?></td>
            <td style="text-align:left;">
                <h3><?php if(isset($getprofile)){ echo $getprofile->fullname; }?></h3>
                SSC# Roll- <?php echo $applicant->ssc_reg; ?>. Reg-<?php echo $applicant->ssc_reg; ?>. <b>GPA-<?php echo $applicant->ssc_result; ?></b>. 
                  <?php echo $applicant->ssc_board; ?>_<?php echo $applicant->ssc_session; ?>)<br/>
                  HSC# Roll- <?php echo $applicant->hsc_reg; ?>. Reg-<?php echo $applicant->hsc_reg; ?>. <b>GPA-<?php echo $applicant->hsc_result; ?></b>.
                  <?php echo $applicant->hsc_board; ?>_<?php echo $applicant->hsc_session; ?>)<br/>
            </td>
            <td style="line-height: 15vh;">
                <?php if($applicant->marks == null){ ?>
                <a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#addmarks<?php echo $applicant->id; ?>">
                    <i class="fa fa-pencil" aria-hidden="true"></i> Marks
                </a>
                    <!-- Modal -->
                    <form action="<?php echo base_url('index.php/Administration/MarksUpdate'); ?>" method="post">
                    <div class="modal fade" id="addmarks<?php echo $applicant->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="myModalLabel" style="text-align:left;">
                              <?php echo $getprofile->fullname; ?>
                            </h3>
                          </div>
                          <div class="modal-body">
                            <input type="hidden" name="id" value="<?php echo $applicant->id; ?>">
                            <input type="text" name="marks" class="form-control" placeholder="Enter Marks">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Add Marks</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    </form>
                <?php }else{ echo $applicant->marks; } ?></td>
        </tr> 
<?php 
    }//applicant list..
?>
    </tbody>
</table>
</div>