<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login | Hall Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background: white; margin:0px; padding:0px;">
	<div class="col-sm-12 col-md-1 col-lg-1"></div>
	<div class="col-sm-12 col-md-10 col-lg-10">
<!--login form-->
<form class="form-horizontal" role="form" action="<?php echo base_url('index.php/Login/saveStudent'); ?>" method="post" style="margin:2% 0%;">

	<div class="user-icon">
		<span class="left-line">.</span>
		<i class="fa fa-user-o" aria-hidden="true"></i> Registration Form for Student
		<span class="right-line">.</span>
      <p class="alert alert-danger" style="color:red; text-align:center;">Without Input Data Don't Reload Or Submit !</p>
	</div><hr/>

	  <input name="userid" type="hidden" value="<?php $userid = $this->session->flashdata('userid'); if(isset($userid)){ echo $userid;  } ?>">

  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">First Name</label>
    <div class="col-sm-4">
      <input type="text" name="firstname" class="form-control" id="firstname" placeholder="First Name">
    </div>
    <label for="lastname" class="col-sm-2 control-label">Last Name</label>
    <div class="col-sm-4">
      <input type="text" name="lastname" class="form-control" id="lastname" placeholder="Username">
    </div>
  </div>
  <div class="form-group">
    <label for="fullname" class="col-sm-2 control-label">Full Name</label>
    <div class="col-sm-10">
      <input type="text" name="fullname" class="form-control" id="fullname" placeholder="Usrname">
    </div>
  </div>

  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">Email</label>
    <div class="col-sm-4">
      <input type="text" name="email" class="form-control" id="email" placeholder="Username">
    </div>
    <label for="phone" class="col-sm-2 control-label">Phone</label>
    <div class="col-sm-4">
      <input type="text" name="phone" class="form-control" id="phone" placeholder="phone">
    </div>
  </div>

  
  <hr/><h4>Family Information</h4><hr/>

  <div class="form-group">
    <label for="father_name" class="col-sm-2 control-label">Father's Name</label>
    <div class="col-sm-2">
      <input type="text" name="father_name" class="form-control" id="father_name" placeholder="e.g.- Mr. Joen Due">
    </div>
    <label for="father_occupation" class="col-sm-2 control-label">Occupation</label>
    <div class="col-sm-2">
      <input type="text" name="father_occupation" class="form-control" id="father_occupation" placeholder="e.g.- Govt. Employee">
    </div>
    <label for="father_income" class="col-sm-2 control-label">Yearly Income</label>
    <div class="col-sm-2">
      <input type="text" name="father_income" class="form-control" id="father_income" placeholder=".tk">
    </div>
  </div>

  <div class="form-group">
    <label for="mother_name" class="col-sm-2 control-label">Mother's Name</label>
    <div class="col-sm-2">
      <input type="text" name="mother_name" class="form-control" id="inputPassword3" placeholder="e.g.- Mrs. Joen Due">
    </div>
    <label for="mother_occupation" class="col-sm-2 control-label">Occupation</label>
    <div class="col-sm-2">
      <input type="text" name="mother_occupation" class="form-control" id="mother_occupation" placeholder="e.g.- Type">
    </div>
    <label for="mother_income" class="col-sm-2 control-label">Yearly Income</label>
    <div class="col-sm-2">
      <input type="text" name="mother_income" class="form-control" id="mother_income" placeholder=".tk">
    </div>
  </div>

  <div class="form-group">
    <label for="birthdate" class="col-sm-2 control-label">Date of Birth</label>
    <div class="col-sm-2">
      <input type="date" name="birthdate" class="form-control" id="birthdate" placeholder="e.g.- Mrs. Joen Due">
    </div>
    <label for="gender" class="col-sm-2 control-label">Gender</label>
    <div class="col-sm-4">
        <div class="radio-inline">
          <label>
            <input type="radio" name="gender" id="optionsRadios1" value="Male">Male
          </label>
        </div>
        <div class="radio-inline">
          <label>
            <input type="radio" name="gender" id="optionsRadios2" value="Female">
            Female
          </label>
        </div>
    </div>
  </div>


  <div class="form-group">
    <label for="paddress" class="col-sm-2 control-label">Present Address</label>
    <div class="col-sm-4">
      <textarea name="address1" id="paddress" class="form-control" rows="4" placeholder="Enter Present Address"></textarea>
    </div>
    <label for="paraddress" class="col-sm-2 control-label">Parmanent Address</label>
    <div class="col-sm-4">
      <textarea name="address2" id="paraddress" class="form-control" rows="4" placeholder="Enter Parmanent Address"></textarea>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" ><i class="fa fa-sign-in" aria-hidden="true"></i> APPLY NOW !</button>
    </div>
  </div>
</form>

	</div>
	<div class="col-sm-12 col-md-1 col-lg-1"></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  </body>
</html>