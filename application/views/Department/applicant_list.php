<div class="table-responsive">
<table id="mytableId" class="table table-bordered table-responsive">
    <thead>
        <tr style="color:white;">
            <th style="width:;text-align: center;">SL</th>
            <th style="width:;text-align: center;">Applicant Info</th>
            <th style="width:;text-align: center;">Education</th>           
            <th style="width:;text-align: center;">Action</th>
        </tr>
    </thead>
    <tbody>

<?php 
    $i = 0;
    foreach ($applicants as $applicant) {
        $i++; 
        if($applicant->marks != NULL AND $applicant->marks != "Admitted"){
          if($applicant->marks != 'Admitted'){
?>
              <?php 
                  $userid = $applicant->userid; 
                  $getprofile = $this->administration_model->applicantprofile($userid); 
              ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td style="font-size:1.3em;"><b><?php echo $getprofile->fullname; ?></b><br/></td>
            <td style="text-align: left;">
                <b>SSC:</b> Roll- <?php echo $applicant->ssc_roll; ?>&nbsp; 
                     Reg- <?php echo $applicant->ssc_reg; ?>&nbsp; 
                     Board- <?php echo $applicant->ssc_board; ?>&nbsp; 
                     Roll- <?php echo $applicant->ssc_result; ?>
                <br/>
                <b>HSC:</b> Roll- <?php echo $applicant->hsc_roll; ?>&nbsp; 
                     Reg- <?php echo $applicant->hsc_reg; ?>&nbsp; 
                     Board- <?php echo $applicant->hsc_board; ?>&nbsp; 
                     Roll- <?php echo $applicant->hsc_result; ?>

            </td>
            <td style="line-height: 5vh;">
                <span class="alert alert-sm alert-success">Marks: <?php echo $applicant->marks; ?></span>&nbsp; &nbsp; 

                <a href="" class="btn btn-sm btn-info" data-toggle="modal" data-target="#confirmadmission<?php echo $applicant->id; ?>">Confirm Admission</a>
              <!--<a href="<?php echo base_url('index.php/Administration/Decline'); ?>/<?php echo $applicant->id; ?>" class="btn btn-sm btn-danger">Reject</a>-->
                    <!-- Modal -->
                    <form action="<?php echo base_url('index.php/Department/AddStudent'); ?>" method="post">
                    <div class="modal fade" id="confirmadmission<?php echo $applicant->id; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header" style="background: teal; color:white;">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            <h3 class="modal-title" id="myModalLabel" style="text-align:left;"><?php echo $getprofile->fullname; ?></h3>
                          </div>
                          <div class="modal-body" style="text-align: left;">
                            <input type="hidden" name="userid" value="<?php echo $applicant->userid; ?>">
                            <br/>
                            Select Department<br/>
                            <select name="department" class="form-control" required="">
                              <option value="">Select Department !</option>
                              <option value="Computer Science & Engineering -CSE">Computer Science & Engineering -CSE</option>
                              <option value="Bachelor of Business Administration - BBA">Bachelor of Business Administration - BBA</option>
                              <option value="Physics">Physics</option>
                              <option value="Applied Physics">Applied Physics</option>
                              <option value="Chemistry">Applied Chemistry</option>
                              <option value="Statistics">Statistics</option>
                              <option value="Biology">Biology</option>
                              <option value="Mathematics">Mathematics</option>
                              <option value="Applied Mathematics">Applied Mathematics</option>
                              <option value="Accounting">Accounting</option>
                              <option value="Finance & Banking">Finance & Banking</option>
                              <option value="Management">Management</option>
                              <option value="Population Science">Population Science</option>
                              <option value="Sociology">Sociology</option>
                            </select>
                            <br/>
                            Session<br/>
                            <input name="session" type="text" class="form-control" placeholder="Enter Session" required=""><br/>

                            Code No. / ID No.<br/>
                            <input name="idno" type="text" class="form-control" placeholder="Enter Code" required="">
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Confirm Admission</button>
                          </div>
                        </div><!-- /.modal-content -->
                      </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    </form>
             </td>

        </tr> 
<?php   
        }//FILTER OF ADMITTED STUDENTS..
      }//END OF MARKS NULL
    }//END OF FOREACH ..applicant list..
?>


    </tbody>
</table>
</div>