<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login | Hall Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background: white; margin:0px; padding:0px;">
	<div class="col-sm-12 col-md-1 col-lg-1"></div>
	<div class="col-sm-12 col-md-10 col-lg-10">
<!--login form-->
<form class="form-horizontal" role="form" action="<?php echo base_url('index.php/Studentoptions/SaveApplication'); ?>" method="post" style="margin:2% 0%;">

	<div class="user-icon">
		<span class="left-line">.</span>
		<i class="fa fa-user-o" aria-hidden="true"></i> Application Form
		<span class="right-line">.</span>
	</div><hr/>
	  <input name="userid" type="hidden" value="<?php echo $this->session->id;  ?>">

  <hr/><h4>Secondary School Certificate-SSC</h4><hr/>
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">SSC ROLL</label>
    <div class="col-sm-4">
      <input type="text" name="ssc_roll" class="form-control" id="firstname" placeholder="">
    </div>
    <label for="lastname" class="col-sm-2 control-label">SSC REG</label>
    <div class="col-sm-4">
      <input type="text" name="ssc_reg" class="form-control" id="lastname" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label">SSC BOARD</label>
    <div class="col-sm-4">
      <select class="form-control" name="ssc_board">
        <option value="">Select Boad ! </option>
        <option value="DHAKA">DHAKA</option>
        <option value="RAJSHAHI">RAJSHAHI</option>
        <option value="CHITTAGONG">CHITTAGONG</option>
        <option value="KHULNA">KHULNA</option>
        <option value="DINAJPUR">DINAJPUR</option>
        <option value="COMILLA">COMILLA</option>
        <option value="BARISHAL">BARISHAL</option>
        <option value="SYLHET">SYLHET</option>
        <option value="MADRASHA">MADRASHA</option>
      </select>
    </div>
    <label for="lastname" class="col-sm-2 control-label">SSC SESSION</label>
    <div class="col-sm-4">
      <input type="text" name="ssc_session" class="form-control" id="lastname" placeholder="">
    </div>
  </div>

  <div class="form-group">
    <label for="email" class="col-sm-2 control-label">SSC GPA</label>
    <div class="col-sm-4">
      <input type="text" name="ssc_result" class="form-control" id="email" placeholder="">
    </div>
    <label for="phone" class="col-sm-2 control-label">SSC GRADE</label>
    <div class="col-sm-4">
      <input type="text" name="ssc_grade" class="form-control" id="phone" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="paddress" class="col-sm-2 control-label">SUBJECT COVERED</label>
    <div class="col-sm-10">
      <textarea name="ssc_subject" id="paddress" class="form-control" rows="2" placeholder=""></textarea>
    </div>
  </div>


  <hr/><h4>Higher Secondary School Certificate-HSC</h4><hr/>
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label"> ROLL</label>
    <div class="col-sm-4">
      <input type="text" name="hsc_roll" class="form-control" id="firstname" placeholder="">
    </div>
    <label for="lastname" class="col-sm-2 control-label"> REG</label>
    <div class="col-sm-4">
      <input type="text" name="hsc_reg" class="form-control" id="lastname" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="firstname" class="col-sm-2 control-label"> BOARD</label>
    <div class="col-sm-4">
      <select class="form-control" name="hsc_board">
        <option value="">Select Boad ! </option>
        <option value="DHAKA">DHAKA</option>
        <option value="RAJSHAHI">RAJSHAHI</option>
        <option value="CHITTAGONG">CHITTAGONG</option>
        <option value="KHULNA">KHULNA</option>
        <option value="DINAJPUR">DINAJPUR</option>
        <option value="COMILLA">COMILLA</option>
        <option value="BARISHAL">BARISHAL</option>
        <option value="SYLHET">SYLHET</option>
        <option value="MADRASHA">MADRASHA</option>
      </select>
    </div>
    <label for="lastname" class="col-sm-2 control-label"> SESSION</label>
    <div class="col-sm-4">
      <input type="text" name="hsc_session" class="form-control" id="lastname" placeholder="">
    </div>
  </div>

  <div class="form-group">
    <label for="email" class="col-sm-2 control-label"> GPA</label>
    <div class="col-sm-4">
      <input type="text" name="hsc_result" class="form-control" id="email" placeholder="">
    </div>
    <label for="phone" class="col-sm-2 control-label"> GRADE</label>
    <div class="col-sm-4">
      <input type="text" name="hsc_grade" class="form-control" id="phone" placeholder="">
    </div>
  </div>
  <div class="form-group">
    <label for="paddress" class="col-sm-2 control-label">SUBJECT COVERED</label>
    <div class="col-sm-10">
      <textarea name="hsc_subject" id="paddress" class="form-control" rows="2" placeholder="Enter Present Address"></textarea>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" ><i class="fa fa-sign-in" aria-hidden="true"></i> APPLY NOW !</button>
      <a href="<?php echo base_url('index.php/Login/'); ?>" class="btn btn-default" ><i class="fa fa-sign-out" aria-hidden="true"></i> Back Login Page !</a>
       	<?php $msg = $this->session->flashdata('msg'); if(isset($msg)){ echo $msg; } ?><!--===Confirmation message==-->
    </div>
  </div>
</form>

	</div>
	<div class="col-sm-12 col-md-1 col-lg-1"></div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  </body>
</html>