  <?php   $role = $this->session->role; ?>
  <body>
	<div class="col-sm-12 col-md-3 col-lg-3" style="background: rgba(0,0,0,0.9); padding-bottom:3%;">
<nav class="navbar navbar-default sidebar" role="navigation" style="border:none;">
      <div style="text-align:center;">
        <span id="logo">e-<i class="fa fa-h-square" aria-hidden="true"></i>ALL</span> 
        <h2 style="margin-top:0px;">Management System</h2><hr/>
      </div>
    <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-sidebar-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>      
    </div>
    <div class="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li style="border:none;"><a href="<?php echo base_url('index.php/Dashboard'); ?>" >Home<span style="font-size:16px;" class="pull-right hidden-xs showopacity glyphicon glyphicon-home"></span></a></li>

<?php if($role == 'Admin' OR $role == 'Author'){ ?>        
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Administration's Options <span class="caret"></span><i class="fa fa-credit-card pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="<?php echo base_url('index.php/Administration/ApplicantList'); ?>">
              Applicant Request
            </a></li>
            <li><a href="<?php echo base_url('index.php/Administration/StudentList'); ?>">Student List</a></li>
            <li><a href=""><!--Employee List</a></li>
            <li><a href="">Teacher List--></a></li>
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin' OR $role == 'Department'){ ?>  
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Department's <span class="caret"></span><i class="fa fa-user-secret pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="<?php echo base_url('index.php/Department/ApplicantList'); ?>">Applicant List </a></li>
            <li><a href=""><!--Teacher List</a></li>
            <li><a href="">Resident/Non-Resident Teacher--></a></li>
            <li><a href="">Student List</a></li>
            <li><a href=""><!--Exam Notice Control</a></li>
            <li><a href="">Result--></a></li>
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin' OR $role == 'Provost'){ ?>  
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Provost <span class="caret"></span><i class="fa fa-users pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="">Resident Confirm</a></li>
            <li><a href="">Student List</a></li>
            <li><a href="">Reports</a></li> 
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin' OR $role == 'Canteen Manager'){ ?> 
         <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Canteen Manage <span class="caret"></span><i class="fa fa-credit-card pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="">Bill Checkout</a></li>
            <li><a href="">Student List</a></li>
            <li><a href="">Report</a></li>
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin' OR $role == 'Librarian'){ ?> 
         <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Library Manage <span class="caret"></span><i class="fa fa-credit-card pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="">Book List & Add Book</a></li>
            <li><a href="">Issue Book</a></li>
            <li><a href="">Issue List</a></li>
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin' OR $role == 'Accounce'){ ?> 
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Account's Option <span class="caret"></span><i class="fa fa-credit-card pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="">Add Payment & Payment List</a></li>
            <li><a href="">Donation</a></li>
            <li><a href="">Salary</a></li>
            <li><a href="">Expence List & Add Expence</a></li>
            <li><a href="">Account's Report</a></li>
          </ul>
        </li> 
<?php } ?>

<?php if($role == 'Admin'){ ?>
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">User Control Option's <span class="caret"></span><i class="fa fa-user pull-right" aria-hidden="true"></i></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="<?php echo base_url('index.php/Userrole/UserList'); ?>">User List</a></li> 
            <li><a href="">User Role</a></li>
          </ul>
        </li> 
<?php } ?>        

<!--
        <li class="dropdown" style="border:none;">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Employee Management<span class="caret"></span><i class="fa fa-users pull-right" aria-hidden="true"></i></span></a>
          <ul class="dropdown-menu forAnimate" role="menu">
            <li><a href="">Employee List & Add Employee</a></li>
            <li><a href="">Resident / Non-Resident</a></li>
            <li><a href="">Sallery Issue</a></li>
            <li><a href="">Employee Reports</a></li>
          </ul>
        </li> 
-->
      </ul>
    </div>
  </div>
      <div style="text-align:center;">
        <p><hr/>
          Powered By- <span style="color:#3276B1; opacity:0.7;">e-Hall Management System</span><br/>
          copyright &copy; 2017
        </p>
      </div>
</nav>

     
  </div>
  
	<div class="col-sm-12 col-md-9 col-lg-9">
    <div class="row top-menu">
      <span style="color:white;">   
        <?php 
            echo $this->session->userdata('username'); 
        ?>
        </span> &nbsp; &nbsp; 
      <a href="<?php echo base_url('index.php/Login/Logout'); ?>" style="text-decoration: none;"><i class="fa fa-sign-out" aria-hidden="true"></i> Logout</a>
    </div>
    <div class="content row">