<div class="table-responsive">
<table id="mytableId" class="table table-bordered table-responsive">
  <style>th{ text-align:center; }</style>
    <thead>
        <tr style="color:white;">
            <th style="width:;">SL</th>
            <th style="width:;">Username</th>
            <th style="width:;">Role</th>            
            <th style="width:;">###</th>
        </tr>
    </thead>
    <tbody>
<?php 
  $i = 0; 
  foreach ($users as $user) {
    $i++; 
    if($user->role != 'Admin' AND $user->role != 'Student'){
?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $user->username; ?></td>
            <td style="background: rgba(0,0,0,0.3);">
              <div class="btn-group">

                <a href="<?php echo base_url('index.php/Userrole/Author/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Author'){ echo 'default'; }else{ echo 'danger'; }?>">Author</a>
                <a href="<?php echo base_url('index.php/Userrole/Accounce/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Accounce'){ echo 'default'; }else{ echo 'danger'; }?>">Accounce</a>
                <a href="<?php echo base_url('index.php/Userrole/Department/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Department'){ echo 'default'; }else{ echo 'danger'; }?>">Department</a>
                <a href="<?php echo base_url('index.php/Userrole/Provost/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Provost'){ echo 'default'; }else{ echo 'danger'; }?>">Provost</a>
                <a href="<?php echo base_url('index.php/Userrole/Canteen/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Canteen Manager'){ echo 'default'; }else{ echo 'danger'; }?>">Canteen Manager</a>
                <a href="<?php echo base_url('index.php/Userrole/Librarian/'); ?><?php echo $user->id; ?>" class="btn btn-<?php if($user->role == 'Librarian'){ echo 'default'; }else{ echo 'danger'; }?>">Librarian</a>
              </div>
            </td>
            <td>
              <a href="<?php echo base_url('index.php/Userrole/ShowDetails/'); ?><?php echo $user->id; ?>" class="btn btn-sm btn-primary">Details</a>
            </td>
        </tr> 
<?php 
    }//end of if.. for admin filter..
  }//END OF FOREACH..
?>
 

    </tbody>
</table>
</div>