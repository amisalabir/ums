<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login | Hall Management System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap -->
    <link href="<?php echo base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body style="background: white;">
	<div class="col-sm-12 col-md-4 col-lg-4"></div>
	<div class="col-sm-12 col-md-4 col-lg-4">

<!--login form-->
<form class="form-horizontal login-form" role="form" action="<?php echo base_url('index.php/Login/UserLoginForm'); ?>" method="post">
	<div class="user-icon">
		<span class="left-line">.</span>
		<i class="fa fa-user-o" aria-hidden="true"></i> Login Form
		<span class="right-line">.</span>
	</div><hr/>
	
  <div class="form-group">
    <label for="inputEmail3" class="col-sm-2 control-label">Username</label>
    <div class="col-sm-10">
      <input type="text" name="username" class="form-control" id="inputEmail3" placeholder="Username">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
    <div class="col-sm-10">
      <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-primary" ><i class="fa fa-sign-in" aria-hidden="true"></i> Login</button>
      <a href="" data-toggle="modal" data-target="#myModal" class="btn btn-danger">Register Now !</a>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
          <?php $msg = $this->session->flashdata('msg'); if(isset($msg)){ echo $msg; } ?><!--===Confirmation message==-->
  </div>
</form>

	</div>
	<div class="col-sm-12 col-md-4 col-lg-4"></div>

<!-- Modal -->

<form role="form" action="<?php echo base_url('index.php/Login/RegisterForm'); ?>" method="post">

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background: teal;">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Register Now !</h4>
      </div>
      <div class="modal-body">
<!---->

  <div class="form-group">
    <label for="exampleInputEmail1">Username.</label>
    <input name="username" type="text" class="form-control" id="exampleInputEmail1" placeholder="Enter Username">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Password.</label>
    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Enter Password">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Confirm Password.</label>
    <input name="cpassword" type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Register As. </label>
    <select name="role" class="form-control">
      <option value="">Select User Type !</option>
      <option value="Author">Author</option>
      <option value="Student">Student</option>
    </select>
  </div>

<!---->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Create Profile</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal --
</form>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://code.jquery.com/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
  </body>
</html>