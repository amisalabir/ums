-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2017 at 06:37 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `database_ums`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_account`
--

CREATE TABLE `tb_account` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `debit` varchar(255) NOT NULL,
  `credit` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_applicant`
--

CREATE TABLE `tb_applicant` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `ssc_roll` varchar(255) NOT NULL,
  `ssc_reg` varchar(255) NOT NULL,
  `ssc_board` varchar(255) NOT NULL,
  `ssc_session` varchar(255) NOT NULL,
  `ssc_result` varchar(255) NOT NULL,
  `ssc_subject` text NOT NULL,
  `hsc_roll` varchar(255) NOT NULL,
  `hsc_reg` varchar(255) NOT NULL,
  `hsc_board` varchar(255) NOT NULL,
  `hsc_session` varchar(255) NOT NULL,
  `hsc_result` varchar(255) NOT NULL,
  `hsc_subject` text NOT NULL,
  `marks` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_applicant`
--

INSERT INTO `tb_applicant` (`id`, `userid`, `ssc_roll`, `ssc_reg`, `ssc_board`, `ssc_session`, `ssc_result`, `ssc_subject`, `hsc_roll`, `hsc_reg`, `hsc_board`, `hsc_session`, `hsc_result`, `hsc_subject`, `marks`) VALUES
(8, '47', '1000171', '12221', 'DHAKA', '2014-15', '4.90', 'ENGLISH, BANGLA, MATH, PHYSICS', '77701', '10098', 'RAJSHAHI', '2018-19', '5', 'ENGLISH, BANGLA, MATH, PHYSICS', 'Admitted'),
(9, '49', '77778898', '8787878', 'CHITTAGONG', '4949-94', '4.90', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '777788787', '9089987878787', 'RAJSHAHI', '2018-19', '3.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', 'Admitted'),
(10, '48', '10000178', '123432100', 'RAJSHAHI', '4949-94', '4.90', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '3434343999', '0987657777', 'KHULNA', '2018-19', '3.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '92'),
(11, '50', '120009', '3400098', 'DHAKA', '2014-15', '2.00', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS ', '1300089', '4700098', 'DHAKA', '2016-17', '5.00', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '45'),
(12, '51', '800012', '988802', 'DHAKA', '2015-16', '4.90', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS ', '222999', '222000', 'RAJSHAHI', '2013-19', '3.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '20'),
(13, '52', '700077', '80088', 'RAJSHAHI', '2014-15', '5.00', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '300033', '400044', 'DINAJPUR', '2012-13', '2.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS ', ''),
(14, '53', '88888', '88888', 'RAJSHAHI', '2015-16', '4.90', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '77701', '88801', 'RAJSHAHI', '2018-19', '4.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', ''),
(15, '54', '5656777', '802333', 'RAJSHAHI', '1234-12', '4.90', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS', '555501', '6678770', 'RAJSHAHI', '2018-19', '3.50', 'ENGLISH, BANGLA, MATHEMATICS, BIOLOGY, PHYSICS ', 'Admitted');

-- --------------------------------------------------------

--
-- Table structure for table `tb_canteen`
--

CREATE TABLE `tb_canteen` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `foods` varchar(255) NOT NULL,
  `bills` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_checkout`
--

CREATE TABLE `tb_checkout` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `checkout` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_food`
--

CREATE TABLE `tb_food` (
  `id` int(11) NOT NULL,
  `foodname` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_hall`
--

CREATE TABLE `tb_hall` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `hallname` varchar(255) NOT NULL,
  `floorno` varchar(255) NOT NULL,
  `roomno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile`
--

CREATE TABLE `tb_profile` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `father_name` varchar(255) NOT NULL,
  `father_occupation` varchar(255) NOT NULL,
  `father_income` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `mother_name` varchar(255) NOT NULL,
  `mother_occupation` varchar(255) NOT NULL,
  `mother_income` varchar(255) NOT NULL,
  `birthdate` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `address1` text NOT NULL,
  `address2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_profile`
--

INSERT INTO `tb_profile` (`id`, `userid`, `firstname`, `lastname`, `fullname`, `email`, `phone`, `father_name`, `father_occupation`, `father_income`, `mother_name`, `mother_occupation`, `mother_income`, `birthdate`, `gender`, `address1`, `address2`) VALUES
(19, '47', 'HARUN', 'BISWASH', 'HARUN BISWAS', 'harun@mail.com', '098980998989', 'A. Rahman', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-20', 'Male', 'NA/', 'N/A'),
(20, '48', 'AKRAM', 'KHAN', 'AKRAM KHAN', 'islam2311@live.com', '+009 9090 090909', 'A. Rahman', 'Business', '0000-00-00 00:00:00', 'MOTHER01', 'House Holder', 'N/A', '2017-12-14', 'Male', 'N/A', 'N/A'),
(21, '49', 'BELAL', 'HOSEN', 'BELAL HOSEN', 'student@one.com', '+998 8989 767676', 'A. Rahman1', 'GOVT. EMPLOYEE', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-20', 'Male', 'NA/', 'N/A'),
(22, '50', 'SAIFUL', 'ISLAM', 'SAIFUL ISLAM', '2323@mail.com', '+990 9090 909090', 'Abdullah', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-13', 'Male', 'DHAKA, BANGLADESH', 'DHAKA, BANGLADESH'),
(23, '51', 'MUNIR', 'HOSEN', 'MUNIR HOSEN', 'islam2311@live.com', '+009 9090 090909', 'A. Rahman', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-19', 'Male', 'NOWAKHALI', 'CHADPUR'),
(24, '52', 'RIPON', 'MOLLIK', 'RIPON MOLLIK', 'r@rr.com', '+009 09 8987777', 'Abdullah', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-27', 'Male', 'WASHINGTON', 'SINGAPOUR'),
(25, '53', 'POLLOBI', 'NILA', 'POLLOBI NILA', 'student@one.com', '+009 9090 090909', 'FATHER 01', 'GOVT. EMPLOYEE', '0000-00-00 00:00:00', 'Halima Khatun', 'TEACHER', '90900', '2017-12-28', 'Male', 'CHITTAGONG', 'DHAKA'),
(26, '54', 'SORNA', 'SEIKH', 'SORNA SEIKH', 'islam2311@live.com', '+009 9090 090909', 'A. Rahman', 'Business', '0000-00-00 00:00:00', 'Halima Khatun', 'House Holder', 'N/A', '2017-12-20', 'Female', 'DHANMONDI, DHAKA', 'DHANMONDI, DHAKA'),
(27, '55', 'ABDUL', 'HAKIM', 'ABDUL HAKIM', 'hakim@mail.com', '+880 9898 765432', 'A. Rahman', 'N/A', '0000-00-00 00:00:00', 'Halima Khatun', 'N/A', 'N/A', '2017-12-12', 'Male', 'JAHANGIR NOGOR, SAVAR, DHAKA', 'N/A'),
(28, '56', 'MOKBUL', 'MOLLA', 'MOKBUL MOLLA', 'mokbul@mail.com', '+880 9876 543212', 'HARUN OR RASHID', 'N/A', '0000-00-00 00:00:00', 'SUFIA SULTAN', 'N/A', 'N/A', '2014-05-14', 'Male', 'TIKATULI, DHAKA', 'N/A'),
(29, '59', 'Farhana', 'Alam', 'Farhana Alam', 'farhana@gmail.com', '0153366522', 'Mother', 'N/A', '0000-00-00 00:00:00', 'Father', 'N/A', 'N/A', '20001-01-01', 'Female', 'Feni', 'Noakhali');

-- --------------------------------------------------------

--
-- Table structure for table `tb_student`
--

CREATE TABLE `tb_student` (
  `id` int(11) NOT NULL,
  `userid` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `session` varchar(255) NOT NULL,
  `idno` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_student`
--

INSERT INTO `tb_student` (`id`, `userid`, `department`, `session`, `idno`) VALUES
(22, '47', 'Computer Science & Engineering -CSE', '2017-18', 'CSE-001'),
(23, '54', 'Statistics', '2015-16', 'STS-001'),
(24, '49', 'Bachelor of Business Administration - BBA', '2017', '25');

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id`, `username`, `password`, `role`) VALUES
(1, 'Admin', 'a428baad1cf55126217738f7f9d8c5ab', 'Admin'),
(47, 'HARUN', '380458d3f1f68aea19b2e0047b37482b', 'Student'),
(48, 'AKRAM', '744fc50fafa9507ea50db2dafc480b62', 'Student'),
(49, 'BELAL', '23c00c62b1a98b76c18a823015ed8d0d', 'Student'),
(50, 'SAIFUL', 'ecb6acbbd5487083fd326d7182f873b5', 'Student'),
(51, 'MUNIR', 'a16ba681405f193fe81a313551af7cc5', 'Student'),
(52, 'RIPON', 'be677badcdf08dab598d0ee8253d5a5f', 'Student'),
(53, 'POLLOBI', 'bdb63f95578409ed297525d1f2a5c313', 'Student'),
(54, 'SORNA', 'ceafc9c6e6bff05a1e5799efd0deece9', 'Student'),
(55, 'ABDUL', 'a428baad1cf55126217738f7f9d8c5ab', 'Author'),
(56, 'MOKBUL', '4e62fb9a7d3f3269208db680c421c1a3', 'Author'),
(57, 'ALAM', 'a428baad1cf55126217738f7f9d8c5ab', 'Department'),
(58, 'amisalabir', 'a428baad1cf55126217738f7f9d8c5ab', 'Author'),
(59, 'farhana', 'a428baad1cf55126217738f7f9d8c5ab', 'Author');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_account`
--
ALTER TABLE `tb_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_applicant`
--
ALTER TABLE `tb_applicant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_canteen`
--
ALTER TABLE `tb_canteen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_food`
--
ALTER TABLE `tb_food`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_hall`
--
ALTER TABLE `tb_hall`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_profile`
--
ALTER TABLE `tb_profile`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `tb_student`
--
ALTER TABLE `tb_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_account`
--
ALTER TABLE `tb_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_applicant`
--
ALTER TABLE `tb_applicant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tb_canteen`
--
ALTER TABLE `tb_canteen`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_checkout`
--
ALTER TABLE `tb_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_food`
--
ALTER TABLE `tb_food`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_hall`
--
ALTER TABLE `tb_hall`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_profile`
--
ALTER TABLE `tb_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `tb_student`
--
ALTER TABLE `tb_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
